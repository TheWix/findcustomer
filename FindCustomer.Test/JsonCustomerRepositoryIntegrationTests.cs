﻿using System.IO;
using NUnit.Framework;

namespace FindCustomer.Test
{
    // NOTE: If you run these in a test runner that supports shadow-copy you may need to turn it off. 
    // It will change the execution location and may not be able to find the customer file.
    public class JsonCustomerRepositoryIntegrationTests
    {
        [Test]
        public void LoadFileHappyPath()
        {
            var testExecutionDirectory = TestContext.CurrentContext.TestDirectory;

            var file = new FileInfo($"{testExecutionDirectory}/customers.txt");
            var customerRepository = new JsonCustomerRepository(file.FullName);

            var customers = customerRepository.GetAll();

            Assert.AreEqual(32, customers.Count);
        }

        [Test]
        public void LoadFileFileNotFound()
        {
            var testExecutionDirectory = TestContext.CurrentContext.TestDirectory;

            var file = new FileInfo($"{testExecutionDirectory}/fileDoesntExist.txt");

            // Some developers aren't a fan of this since it is mixing Act and Assert.
            Assert.Throws<FileNotFoundException>(() => new JsonCustomerRepository(file.FullName));
        }
    }
}
﻿using System;
using NUnit.Framework;

namespace FindCustomer.Test
{
    public class CoordinatesUnitTests
    {
        [TestCase(10, 20, null, Description = "Valid Latitude and Longitude")]
        [TestCase(180, 20, typeof(ArgumentOutOfRangeException), Description = "Latitude > 90")]
        [TestCase(-180, 20, typeof(ArgumentOutOfRangeException), Description = "Latitude < 90")]
        [TestCase(80, 181, typeof(ArgumentOutOfRangeException), Description = "Longitude > -180")]
        [TestCase(80, 181, typeof(ArgumentOutOfRangeException), Description = "Longitude < 180")]
        public void TestConstructionValidationRules(double latitude, double longitude, Type expectedException)
        {
            Type exceptionType = null;

            try
            {
                new Coordinates(latitude, longitude);
            }
            catch (Exception exception)
            {
                exceptionType = exception.GetType();
            }

            if (expectedException != null)
            {
                Assert.AreEqual(expectedException, exceptionType);
            }
        }

        //Confirmed these distances here http://www.movable-type.co.uk/scripts/latlong.html and here http://boulter.com/gps/distance/
        [TestCase(53.345635, -6.2675536, 53.3455975, -6.2703424, .1852,
             Description = "Jameson Distilery to Center of St. Stephen's Green")]
        [TestCase(53.3393, -6.2576841, 37.788802, -122.4025067, 8175,
             Description = "Intercom Dublin Office and Intercom San Francisco Office")]
        public void CalculatingDistanceTest(double latitudeOne, double longitudeOne, double latitudeTwo,
            double longitudeTwo, double expectedDistance)
        {
            var coordinateOne = new Coordinates(latitudeOne, longitudeOne);
            var coordinateTwo = new Coordinates(latitudeTwo, longitudeTwo);

            var distanceApart = coordinateOne.DistanceBetween(coordinateTwo);

            Assert.AreEqual(expectedDistance, distanceApart, .5f); //Equal to within 500 meters
        }
    }
}
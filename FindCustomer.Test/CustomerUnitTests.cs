﻿using System;
using NUnit.Framework;

namespace FindCustomer.Test
{
    public class CustomerUnitTests
    {
        [TestCase("John Doe", 20, null, Description = "Name and id")]
        [TestCase("", 20, typeof(ArgumentException), Description = "Invalid name")]
        [TestCase(null, 20, typeof(ArgumentException), Description = "Invalid name")]
        [TestCase("John Doe", -1, typeof(ArgumentOutOfRangeException), Description = "Invalid id")]
        public void TestConstructionValidationRules(string name, int id, Type expectedException)
        {
            Type exceptionType = null;

            try
            {
                new Customer(id, name, new Coordinates(1, 1));
            }
            catch (Exception exception)
            {
                exceptionType = exception.GetType();
            }

            if (expectedException != null)
            {
                Assert.AreEqual(expectedException, exceptionType);
            }
        }
    }
}
﻿using System;

namespace FindCustomer
{
    public struct Coordinates
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        /// <summary>
        /// Coordinate on Earth
        /// </summary>
        /// <param name="latitude"></param>
        /// <param name="longitude"></param>
        /// <exception cref="ArgumentOutOfRangeException">If latitude is not between 90 and -90 or longitude is not between 180 and -180</exception>
        public Coordinates(double latitude, double longitude)
        {
            if (latitude > 90 || latitude < -90) throw new ArgumentOutOfRangeException(nameof(latitude));
            if (longitude > 180 || longitude < -180) throw new ArgumentOutOfRangeException(nameof(longitude));
            Latitude = latitude;
            Longitude = longitude;
        }

        /// <summary>
        /// Calculates the distance between two coordinates using the Spherical Law of Cosines
        /// </summary>
        /// <param name="otherCoordinates">The second set of <see cref="Coordinates"/> to calculate the distance between </param>
        /// <returns>The distance between the two coordinates in kilometers</returns>
        /// <remarks> If we wanted to use different Distance formulas for whatever reason
        ///  I would maybe extract this function into a service class and use a strategy patterns.
        /// For our app this should suffice.</remarks>
        public double DistanceBetween(Coordinates otherCoordinates)
        {
            const double EarthsRadius = 6371e3;
            var latitudeOneInRadians = Latitude.ToRadians();
            var latituudeTwoInRadians = otherCoordinates.Latitude.ToRadians();
            var longitudeDelta = (otherCoordinates.Longitude - Longitude).ToRadians();
            var result =
                Math.Acos(Math.Sin(latitudeOneInRadians)*Math.Sin(latituudeTwoInRadians) +
                          Math.Cos(latitudeOneInRadians)*
                          Math.Cos(latituudeTwoInRadians)*Math.Cos(longitudeDelta))*EarthsRadius;

            return result/1000; // Meters to Kilometers
        }
    }
}
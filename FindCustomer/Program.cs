﻿using System;
using System.Linq;

namespace FindCustomer
{
    public class Program
    {
        static void Main(string[] args)
        {
            var customerRepository = new JsonCustomerRepository("customers.txt");

            var customers = customerRepository
                .GetBy(Predicates.CustomerWithin100KilometersOfDublinOffice)
                .OrderBy(x => x.Id)
                .ToList();

            customers.ForEach(customer => Console.WriteLine($"{customer.Id}: {customer.Name}"));
        }
    }
}
﻿using System;
using System.Collections.Generic;

namespace FindCustomer
{
    public interface ICustomerRepository
    {
        /// <summary>
        /// Gets a list of all <see cref="Customer"/>
        /// </summary>
        List<Customer> GetAll();

        /// <summary>
        /// Gets a list of <see cref="Customer"/> by a predicate
        /// </summary>
        /// <remarks>I am using a Func here, but we could use an Expression for more advanced querying of customers</remarks>
        List<Customer> GetBy(Func<Customer, bool> predicate);
    }
}
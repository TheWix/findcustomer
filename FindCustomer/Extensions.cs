﻿using System;

namespace FindCustomer
{
    public static class Extensions
    {
        public static double ToRadians(this double val)
        {
            return (Math.PI/180)*val;
        }
    }
}
﻿using System;

namespace FindCustomer
{
    public class Customer
    {
        public int Id { get; }
        public string Name { get; }
        public Coordinates Coordinates { get; }

        /// <summary>
        /// Creates an instance of <see cref="Customer"/>
        /// </summary>
        /// <param name="id">The identifier of the customers</param>
        /// <param name="name">The name of the customer</param>
        /// <param name="latitude">latitude coordinate of where the customer lives</param>
        /// /// <param name="longitude">longitude coordinate of where the customer lives</param>
        /// <exception cref="ArgumentException"><param name="name"></param> is null or empty</exception>
        /// <exception cref="ArgumentOutOfRangeException"><param name="id"></param> is less than 0.</exception>
        public Customer(int id, string name, double latitude, double longitude)
            : this(id, name, new Coordinates(latitude, longitude))
        {
        }

        /// <summary>
        /// Creates an instance of <see cref="Customer"/>
        /// </summary>
        /// <param name="id">The identifier of the customers</param>
        /// <param name="name">The name of the customer</param>
        /// <param name="coordinates"><see cref="Coordinates"/> of where the customer lives</param>
        /// <exception cref="ArgumentException"><param name="name"></param> is null or empty</exception>
        /// <exception cref="ArgumentOutOfRangeException"><param name="id"></param> is less than 0.</exception>
        public Customer(int id, string name, Coordinates coordinates)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentException($"{nameof(name)} cannot be null or empty", nameof(name));
            if (id <= 0) throw new ArgumentOutOfRangeException(nameof(id));

            Coordinates = coordinates;

            Id = id;
            Name = name;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;

namespace FindCustomer
{
    public class JsonCustomerRepository : ICustomerRepository
    {
        private readonly string _customerFile;

        public JsonCustomerRepository(string customerFile)
        {
            if (string.IsNullOrWhiteSpace(customerFile)) throw new ArgumentNullException(nameof(customerFile));
            if (!File.Exists(customerFile)) throw new FileNotFoundException($"Could not locate file {customerFile}");

            _customerFile = customerFile;
        }

        public List<Customer> GetAll()
        {
            // Have to read line by line since file doesn't represent a json array of customers
            var customers =
                File.ReadAllLines(_customerFile)
                    .Select(JsonConvert.DeserializeObject<JsonCustomer>)
                    .Select(
                        x =>
                            new Customer(int.Parse(x.user_id), x.name, double.Parse(x.latitude),
                                double.Parse(x.longitude)))
                    .ToList();

            return customers;
        }

        public List<Customer> GetBy(Func<Customer, bool> predicate)
        {
            return GetAll()
                .Where(predicate)
                .ToList();
        }

        internal class JsonCustomer
        {
            public string latitude { get; set; }
            public string user_id { get; set; }
            public string name { get; set; }
            public string longitude { get; set; }
        }
    }
}
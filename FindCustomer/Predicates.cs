﻿using System;

namespace FindCustomer
{
    /// <summary>
    /// Static class for functional-style functions/predicates.
    /// </summary>
    /// <remarks>I have been getting interested in Functional programming as of late, and like to try out some of the concepts.
    /// They also work very well with LINQ.
    /// </remarks>
    public static class Predicates
    {
        public static readonly Func<Coordinates, Coordinates, float, bool> CoordinatesWithinXKilometers =
            (coordinates1, coordinates2, kilometerThreshold) =>
                    coordinates1.DistanceBetween(coordinates2) <= kilometerThreshold;

        public static readonly Func<Customer, bool> CustomerWithin100KilometersOfDublinOffice =
            cust => CoordinatesWithinXKilometers(cust.Coordinates, new Coordinates(53.3393, -6.2576841), 100);
    }
}